public class Main {
    public static void main(String [] args){
        Person husband = PersonBuilder.builder()
                .name("Muhammed").age(24).build();

        System.out.println(husband.toString());
    }
}
