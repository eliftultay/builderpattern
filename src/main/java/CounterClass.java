public class CounterClass {
    private static int counter = 0;

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        CounterClass.counter = counter;
    }

    public void increase(){
        counter++;
    }
}
